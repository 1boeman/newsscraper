# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

from itemadapter import ItemAdapter
from scrapy.exporters import XmlItemExporter
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import ElementTree
import os

class NewsPipeline:
  def open_spider(self, spider):
    t = spider.name
    self.file = open(t+'.xml', 'wb')
    self.exporter = XmlItemExporter(self.file,item_element='item', root_element='channel')
    self.exporter.start_exporting()

  def close_spider(self, spider):
    self.exporter.finish_exporting()
    self.file.close()
    feed_title = spider.name
    feed_link = spider.start_urls[0]
    feed_description = spider.name
    tree = ET.parse(feed_title + '.xml')
    final_root = ET.fromstring('<rss version="2.0"></rss>') 
    root = tree.getroot()

    t = root.makeelement('link',{})
    t.text = feed_link
    root.insert(0,t)
    
    t = root.makeelement('description',{})
    t.text = feed_title
    root.insert(0,t)
   
    t = root.makeelement('title',{})
    t.text = feed_title
    root.insert(0,t)
    final_root.append(root) 
    output_dir = "/tmp/scrapy_output"
    if not os.path.isdir(output_dir):
      os.mkdir(output_dir)

    ElementTree(final_root).write( output_dir + '/' + feed_title + '.xml')

  def process_item(self, item, spider):
    self.exporter.export_item(item)
    return item
