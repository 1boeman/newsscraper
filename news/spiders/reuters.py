# -*- coding: utf-8 -*-
import scrapy
from news.items import NewsItem

class ReutersSpider(scrapy.Spider):
  name = 'reuters'
  allowed_domains = ['reuters.com']
  start_urls = ['https://www.reuters.com/news/world']

  def parse(self, response):
    for story in response.css('article.story'):
      newsitem = NewsItem()
      newsitem['title'] = story.css('h3.story-title::text').get()
      newsitem['description'] = story.css('p::text').get() 
      newsitem['link'] = 'https://www.reuters.com' + story.css('a')[0].attrib['href']
      yield newsitem
